version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = "mvcturbine"
	spec.version     = version
	spec.files 		 = Dir['lib/**/*']
	spec.summary     = "MVC Turbine - A simple, extensible framework to streamline your ASP.NET MVC development"
	spec.description = "MVC Turbine is a plugin for ASP.NET MVC that has IoC baked in and auto-wires controllers, binders, view engines, http modules, etc. that reside within your application. Thus you worry more about what your application should do, rather than how it should do it."
	spec.author     = "Javier Lozano"
	spec.email       = "jglozano@gmail.com"
	spec.homepage    = "http://mvcturbine.codeplex.com/"
	spec.rubyforge_project = "mvcturbine"
end
