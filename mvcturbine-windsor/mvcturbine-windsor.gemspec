version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = "mvcturbine-windsor"
	spec.version     = version
	spec.files 		 = Dir['lib/**/*']
	spec.summary     = "MVC Turbine (Castle Windsor) - A simple, extensible framework to streamline your ASP.NET MVC development"
	spec.description = "MVC Turbine is a plugin for ASP.NET MVC that has IoC baked in and auto-wires controllers, binders, view engines, http modules, etc. that reside within your application. Thus you worry more about what your application should do, rather than how it should do it. This is the Castle Windsor adapter for MVC Turbine."
	spec.author     = "Javier Lozano"
	spec.email       = "jglozano@gmail.com"
	spec.homepage    = "http://mvcturbine.codeplex.com/"
	spec.rubyforge_project = "mvcturbine-windsor"
	spec.add_dependency('mvcturbine','= 2.1.0.0.20100820')
	spec.add_dependency('castle.windsor','= 2.0.0.0')
end
