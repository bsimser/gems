version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = "ninject"
	spec.version     = version
	spec.files 		 = Dir['lib/**/*']
	spec.summary     = "Ninject is a lightweight dependency injection framework for .NET applications"
	spec.description = "Ninject is a lightweight dependency injection framework for .NET applications. It helps you split your application into a collection of loosely-coupled, highly-cohesive pieces, and then glue them back together in a flexible manner. By using Ninject to support your softwares architecture, your code will become easier to write, reuse, test, and modify."
	spec.author      = "Nate Kohari"
	spec.email       = "nate@enkari.com"
	spec.homepage    = "http://ninject.org/"
	spec.rubyforge_project = "ninject"
end
