version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = 'webformsmvp'
	spec.version     = version
	spec.files 		 = Dir['lib/**/*']
	spec.summary     = 'Web Forms MVP - A simple Model-View-Presenter framework for ASP.NET Web Forms'
	spec.description = <<-EOF
		Web Forms MVP is a simple Model-View-Presenter framework for ASP.NET Web 
		Forms to aid in building testable and maintainable ASP.NET projects. 
		Features support for normal server controls, data-binding and async pages.
	EOF
	spec.authors     = ['Tatham Oddie','Damian Edwards']
	spec.email       = 'webformsmvp@googlegroups.com'
	spec.homepage    = 'http://webformsmvp.com/'
	spec.rubyforge_project = 'webformsmvp'
end
