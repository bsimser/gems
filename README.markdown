Bil Simsers Gems for Nu
=======================

These are my gem files for the [Nu project][nu]. Feel free to take
a look at them to learn how to build a gem file.

There's really no reason to fork this repository as all gems
are just built from the following files:

* VERSION
* [project].gemspec
* additional folders with binaries (lib/doc/etc.)

Then from the command line enter:

	gem build [project].gemspec
	
This will produce the .gem file using the filename [project]-[version].gem where
[version] is the contents of the VERSION file. This is usually (depending on the
gem author) in the format of:

Major.Minor.Build.Revision.YYYYMMMDD
	
* Major: A major version of the project (1, 2, etc.)
* Minor: A minor release off the major version
* Build: Optional. Could be a build number or whatever you want
* Revision: Another optional value.
* YYYYMMDD: The date the gem was published. Append HHMM if you publish the same gem twice in a day.
	
The Major.Minor versions should be made in-sync with the project public release
versions to avoid any confusion.

Example
-------

Project Foo is releasing a new build today (the 10th of October, 2010) and you decide to 
create a gem for it. Once the release is built you start working, building your .gemspec
and getting all the release files. Once you structure the build you find that Project Foo
is using 2.5 for their version number.

The contents of your VERSION file should be:

	2.5.0.0.20101010
	
We add 0 values for the build and revision number to the version so that all our gems
have the n.n.n.n.YYYYMMDD format and not n.n.YYYYMMDD. This is just for consistency but
also provides more granular versioning and comparing releases.

More Information
----------------

For a full example please my blog post [here][myblog] on building gems for .NET.

[myblog]: http://weblogs.asp.net/bsimser/archive/2010/07/30/creating-a-quot-new-quot-gem-for-quot-nu-quot-from-0-to-100-in-24-hours.aspx
[Nu]: http://nu.wikispot.org/Front_Page
