version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = 'autofac'
	spec.version     = version
	spec.files = Dir['lib/**/*'] + Dir['docs/**/*']
	spec.summary     = 'Autofac - An addictive .NET IoC container'
	spec.description = 'Autofac is an IoC container for Microsoft .NET. It manages the dependencies between classes so that applications stay easy to change as they grow in size and complexity. This is achieved by treating regular .NET classes as components.'
	spec.authors           = ['Nicholas Blumhardt','Rinat Abdulin']
	spec.email             = 'emailme@bilsimser.com'
	spec.homepage          = 'http://code.google.com/p/autofac/'
	spec.rubyforge_project = 'autofac'
end
