#!/bin/bash
#
# gemcutter installation script
# mostly derived from @drusellers installation instructions
#
# This script automates the creation of a gemcutter (rubygems.org)
# server in your own Ubuntu environment. Run it from a clean install
# of Ubuntu and you should have a fully working server in about 20 minutes.
#
# The following software will be downloaded, installed, and configured:
# * ruby
# * rubygems
# * git
# * postgresql
# * apache
# * rails
# * passenger
# + many more...
#
# Instructions
# ------------
# 1. Install ubuntu
# 2. Open up a terminal (command prompt)
# 3. wget http://github.com/bsimser/gems/raw/master/gemcutter/install_gemcutter.sh
# 4. chmod +x install_gemcutter.sh
# 5. sudo ./install_gemcutter.sh
# 6. Go for coffee
#
# Known issues
# ------------
# * passenger-install-apache2-module has two prompts to hit enter that I would like to get rid of
#
# The future?
# -----------
# * better (aka some) error handling
# * kittens, everyone loves kittens!
# * apt-get install gemcutter (I can dream can't I?)
#
# For questions, updates or support please contact Bil Simser (email:emailme@bilsimser.com, twitter:@bsimser)

########################
# NOTE: this script is not fully operational yet! It will only
# get you 90% of the way as not everything has been tested or
# automated yet (but it's pretty close)
#
# Not working:
# * postfix (mail server) - haven't automated this yet. apt-get install currently pops up a ui
# * website setup - comes close but fails on some permission issues db:create works but db:migrate fails
########################

#
# begin config options
#
RUBYGEMS_VERSION="1.3.7"
GEMCUTTER_PASSWORD="password"
REDIS_PASSWORD="password"
POSTGRES_PASSWORD="password"
WEBSITE_DIRECTORY="gemapp"
WEBSITE_DOMAIN="gemapp.local"
#
# end config options
#

########################
# YOU SHOULDN'T HAVE TO CHANGE ANYTHING BELOW!!!
########################

# make sure we are root!
clear
if [ "$UID" != "0" ]; then
	echo
	echo -e "\E[1;33mThis installation script must be executed with root privledges! Exiting...\E[32;0m"
	echo
	echo -e "\E[1;33mPlease use the syntax 'sudo ./install_gemcutter.sh' to execute it."
	echo
	exit 1
fi

#
# Random Junk that is needed
#
apt-get install libxslt-dev libxml2-dev --assume-yes

#
# Add Users
#
useradd --create-home -p `openssl passwd -crypt ${GEMCUTTER_PASSWORD}` gemcutter
useradd --create-home -p `openssl passwd -crypt ${REDIS_PASSWORD}` redis

#
# Setting Up Ruby
#
apt-get install ruby-full --assume-yes

#
# Clean up dependencies we don't need anymore
#
apt-get autoremove --assume-yes

#
# Getting Gems on Ubuntu
# apparently apt-get gems sucks
#
wget http://production.cf.rubygems.org/rubygems/rubygems-${RUBYGEMS_VERSION}.tgz
tar zxvf rubygems-${RUBYGEMS_VERSION}.tgz
ruby ./rubygems-${RUBYGEMS_VERSION}/setup.rb
rm -r rubygems-${RUBYGEMS_VERSION}
rm rubygems-${RUBYGEMS_VERSION}.tgz
ln -s /usr/bin/gem1.8 /usr/bin/gem

#
# Install Git
#
apt-get install git-core --assume-yes

#
# Setting Up PostgreSQL
#
apt-get install postgresql --assume-yes
apt-get install postgresql-server-dev-8.4 --assume-yes
sudo -u postgres psql -d template1 -c "ALTER USER postgres WITH PASSWORD '${POSTGRES_PASSWORD}';"
sudo -u postgres psql -c "create user root login SUPERUSER INHERIT CREATEDB CREATEROLE;"
sudo /etc/init.d/postgresql-8.4 stop
sudo rm -r /var/lib/postgresql/8.4/main
sudo -u postgres /usr/lib/postgresql/8.4/bin/initdb -D /var/lib/postgresql/8.4/main --locale=en_US.UTF-8
sed -e "s/ssl = true/#ssl = true/g" -i /etc/postgresql/8.4/main/postgresql.conf
/etc/init.d/postgresql-8.4 start -D /var/lib/postgresql/8.4/main
sudo -u postgres psql -c "CREATE USER gemcutter CREATEDB;"

#
# Get a bunch of gems
#
gem install pg
gem install gemcutter
gem install nokogiri
gem install rails --no-rdoc --pre

# 
# Set Up Redis
#
wget http://github.com/bsimser/gems/raw/master/gemcutter/redis_user.sh
chmod +x redis_user.sh
cp redis_user.sh /home/redis
sudo su - redis -c ./redis_user.sh
mkdir /etc/init.d/redis
wget http://gist.github.com/raw/555701/a7a48c24ef6e14074ce5957f94b9356c79e781f2/redis.conf
mv redis.conf /home/redis/bin
wget http://gist.github.com/raw/555383/5af2d90ff3a09c1e47126ba0df09c939ade6524f/redis.sh
chmod +x redis.sh
mv redis.sh /etc/init.d/redis
sudo su redis -c "/etc/init.d/redis/redis.sh start"

#
# Setting Up Apache
#
apt-get install apache2 --assume-yes
apt-get install build-essential apache2-prefork-dev libapr1-dev libaprutil1-dev --assume-yes
gem install passenger
passenger-install-apache2-module
echo "LoadModule passenger_module /usr/lib/ruby/gems/1.8/gems/passenger-2.2.15/ext/apache2/mod_passenger.so" > /etc/apache2/mods-available/passenger.load
echo "PassengerRoot /usr/lib/ruby/gems/1.8/gems/passenger-2.2.15" > /etc/apache2/mods-available/passenger.conf
echo "PassengerRuby /usr/bin/ruby1.8" >> /etc/apache2/mods-available/passenger.conf
cd /etc/apache2/mods-available
a2enmod passenger
/etc/init.d/apache2 restart

#
# Setup Postfix
#
#this is a pretty dirt simple email server
#the configuration file is located at �/etc/postfix/main.conf�
#sudo apt-get install postfix
#select internet (the default)
#select ${WEBSITE_DOMAIN} (the default)
#sudo apt-get install mailutils

#
# Setup The Website
#
mkdir /var/www/${WEBSITE_DIRECTORY}
cd /var/www/${WEBSITE_DIRECTORY}
git clone http://github.com/rubygems/gemcutter.git .
#TODO set up the vhost.conf
#http://cjohansen.no/en/ruby/setting_up_gitorious_on_your_own_server
#consider putting the conf file in /etc/apache2/sites-available
# then use: sudo a2ensite
#sudo ln -s /var/www/${WEBSITE_DIRECTORY}/conf/vhost.conf /etc/apache2/sites-enabled/${WEBSITE_DIRECTORY}
#/etc/init.d/apache2 restart

cd /var/www/${WEBSITE_DIRECTORY}
bundle install vendor/bundler_gems
cd config
cp database.yml.example database.yml
echo "  username: gemcutter" >> database.yml
echo "  password: ${GEMCUTTER_PASSWORD}" >> database.yml

#wget gemcutter_user.sh
#sudo su gemcutter -c ./gemcutter_user.sh

#run as 'gemcutter' user
#sudo su - gemcutter 
#bundle exec rake db:create RAILS_ENV=production
#TODO Unable to access log file. Please ensure that /var/www/gemapp/log/production.log exists and is chmod 0666
#FIXME Permission denied - /var/www/gemapp/db/schema.rb
#bundle exec rake db:migrate RAILS_ENV=production

#TODO production.rb change
#HOST = ${WEBSITE_DOMAIN}

#had to modify some apache settings. need to better document this

#this is what makes the website run as gemcutter - thank you passenger
#chown gemcutter /var/www/${WEBSITE_DIRECTORY}/gemcutter

#rails s -e production

#rails#set up capistrano instructions
