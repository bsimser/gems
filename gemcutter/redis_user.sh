#!/bin/bash
#
# gemcutter installation script
# supplemental script for setting things up under redis user
#
# For questions, updates or support please contact Bil Simser (emailme@bilsimser.com)

cd
mkdir bin
mkdir data
git clone git://github.com/antirez/redis src
cd src
git reset --hard v2.0.0-rc4
make
cp redis-server ../bin
cp redis-cli ../bin
