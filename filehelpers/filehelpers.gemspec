version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = 'filehelpers'
	spec.version     = version
	spec.files = Dir['lib/**/*'] + Dir['docs/**/*']
	spec.summary     = 'FileHelpers - .NET library to import/export data from fixed length or delimited records'
	spec.description = 'The FileHelpers are a free and easy to use .NET library to import/export data from fixed length or delimited records in files, strings or streams.'
	spec.author           = 'Marcos Meli'
	spec.email             = 'marcos@filehelpers.com'
	spec.homepage          = 'http://www.filehelpers.com/'
	spec.rubyforge_project = 'filehelpers'
end
