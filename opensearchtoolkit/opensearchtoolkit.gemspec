version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = "opensearchtoolkit"
	spec.version     = version
	spec.files 		 = Dir['lib/**/*']
	spec.summary     = "OpenSearch Toolkit - Provides drop-in support for OpenSearch to ASP.NET developers"
	spec.description = "The OpenSearch Toolkit provides ASP.NET developers with drop-in support for OpenSearch. This lets you quickly and easily publish valid search suggestions to all the major browsers."
	spec.author     = "Tatham Oddie"
	spec.email       = "tatham@oddie.com.au"
	spec.homepage    = "http://opensearchtoolkit.codeplex.com/"
	spec.rubyforge_project = "opensearchtoolkit"
end
