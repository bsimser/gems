version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = 'quickgraph'
	spec.version     = version
	spec.files = Dir['lib/**/*'] + Dir['docs/**/*']
	spec.summary     = 'QuickGraph 3.0 - Generic Graph Data Structures and Algorithms for .Net'
	spec.description = 'QuickGraph provides generic directed/undirected graph datastructures and algorithms for .Net 2.0 and up. QuickGraph comes with algorithms such as depth first seach, breath first search, A* search, shortest path, k-shortest path, maximum flow, minimum spanning tree, least common ancestors, etc... QuickGraph supports MSAGL, GLEE, and Graphviz to render the graphs, serialization to GraphML, etc...'
	spec.author           = 'Jonathan de Halleux'
	spec.email             = 'emailme@bilsimser.com'
	spec.homepage          = 'http://quickgraph.codeplex.com/'
	spec.rubyforge_project = 'quickgraph'
end
