version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = "lucene-dotnet"
	spec.version     = version
	spec.files 		 = Dir['lib/**/*']
	spec.summary     = "Lucene.Net is a source code, class-per-class, API-per-API and algorithmatic port of the Java Lucene search engine to the C# and .NET platform utilizing  Microsoft .NET Framework."
	spec.description = <<-EOF
		Lucene.Net is a source code, class-per-class, API-per-API and algorithmatic port of the Java Lucene search engine to the C# 
		and .NET platform utilizing  Microsoft .NET Framework.

		Lucene.Net sticks to the APIs and classes used in the original Java implementation of Lucene. The API names as well as 
		class names are preserved with the intention of giving Lucene.Net the look and feel of the C# language and the .NET 
		Framework. For example, the method Hits.length() in the Java implementation now reads Hits.Length() in the C# port.

		In addition to the APIs and classes port to C#, the algorithm of Java Lucene is ported to C# Lucene. This means an index 
		created with Java Lucene is back-and-forth compatible with the C# Lucene; both at reading, writing and updating. In fact 
		a Lucene index can be concurrently searched and updated using Java Lucene and C# Lucene processes. 		
	EOF
	spec.author     = "George Aroush"
	spec.email       = "george@aroush.net"
	spec.homepage    = "http://lucene.apache.org/lucene.net/"
	spec.rubyforge_project = "lucene-dotnet"
end
