version = File.read(File.expand_path("../VERSION", __FILE__)).strip

Gem::Specification.new do |spec|
	spec.platform    = Gem::Platform::RUBY
	spec.name        = 'elmah'
	spec.version     = version
	spec.files = Dir['lib/**/*'] + Dir['docs/**/*']
	spec.summary     = 'ELMAH - Error Logging Modules and Handlers for ASP.NET'
	spec.description = 'ELMAH (Error Logging Modules and Handlers) is an application-wide error logging facility that is completely pluggable. It can be dynamically added to a running ASP.NET web application, or even all ASP.NET web applications on a machine, without any need for re-compilation or re-deployment.'
	spec.author           = 'Atif Aziz'
	spec.email             = 'atif.aziz@skybow.com'
	spec.homepage          = 'http://code.google.com/p/elmah/'
	spec.rubyforge_project = 'elmah'
end
